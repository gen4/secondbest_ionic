angular.module('starter').controller('HistoryCtrl',['$scope', '$state','$ionicHistory', 'dataLayer','$ionicModal', function($scope, $state, $ionicHistory, dataLayer, $ionicModal) {
  $scope.answers = [];
  var step = 10;
  var offset = 0;
  var moreDataCanBeLoaded = true;

  var detailsScope = $scope.$new();
  detailsScope.betClasses = {
    1:'calm-bg',
    3:'positive-bg',
    10:'royal-bg'
  };
  detailsScope.closeModal = function() {
    detailsScope.modal.hide();
  };

  $scope.showDetails = function(answer){
    console.log('answer' , answer);
    detailsScope.task=answer.task;
    detailsScope.answer=answer;


    detailsScope.modal.show();
  };

  $scope.moreDataCanBeLoaded = function(){
    return moreDataCanBeLoaded;
  }
  $scope.loadMore=function(){
    dataLayer.httpGet('/me/history', false, {offset:offset,limit:step}).then(function(answers){
      if(!answers || !answers.data || !answers.data.length || answers.data.length<=0){
        moreDataCanBeLoaded = false;
      };
      $scope.$broadcast('scroll.infiniteScrollComplete');
      Array.prototype.push.apply($scope.answers,answers.data);
      offset+=step;
    });

  }

  $ionicModal.fromTemplateUrl('templates/history-card-details.modal.html', {
    scope: detailsScope
  }).then(function(modal) {
    detailsScope.modal = modal;
  });

  $scope.loadMore();
}]);
