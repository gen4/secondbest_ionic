angular.module('starter').controller('ProfileCtrl',['$scope','$ionicHistory', '$state', 'dataLayer', 'current_user', 'rating_position', 'purchasesService', 'userService', '$ionicModal','commonService',
  function($scope, $ionicHistory, $state, dataLayer, user,rating_position,purchasesService,userService,$ionicModal,commonService) {

  userService.refreshUserData();
  $scope.user = userService.getUser();
  $scope.hasSocial = _.any($scope.user.regs,function(reg){
    return reg.service != 'naked';
  });

    $ionicModal.fromTemplateUrl('templates/setup.modal.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.setupModal = modal;
    });
    $scope.closeModal = function() {
      $scope.setupModal.hide();
    };

    $scope.showSetup = function(){
      $scope.setupModal.show();
    }

    $scope.addFb = function(){
      dataLayer.addFb(finishAuth);
    };
    $scope.addVk = function(){
      dataLayer.addVk(finishAuth);
    };

  $scope.openLink = function(link){
    window.open(link, '_system', 'location=yes');
  };

    $scope.showHelp = commonService.showHelp;

    function finishAuth(token) {
      userService.setToken(token);
      $scope.setupModal.hide();
    }


  var perc = Math.ceil(rating_position.data.position/rating_position.data.total*100);
  var isTop = false;

  if(perc<=50){
    isTop=true;
  }

  if(perc>10){
    //perc = Math.ceil(perc/10)*10;
    if(perc>50){
      perc-=1;
    }
  }

  $scope.rating_position = (isTop?'TOP ':'Under ') + perc+'%  ('+rating_position.data.position+'/'+rating_position.data.total+')' ;
  console.log('U ',user);


  $scope.order = purchasesService.order;

  $scope.logout = function(){
    $scope.setupModal.hide();
    userService.logout();
  }

}]);
