angular.module('starter').controller('CardsCtrl',['$scope', '$state', 'dataLayer',  '$ionicModal','$rootScope','$ionicPopup','$translate','userService','commonService','fullscreenbannerService','$ionicAnalytics',
  function($scope, $state, dataLayer,  $ionicModal,$rootScope,$ionicPopup, $translate, userService,commonService, fullscreenbannerService,$ionicAnalytics) {


    $scope.user = userService.getUser();

    var imageHost ='http://d1uzzjg24mb7so.cloudfront.net/'


    var betConfig = {
      betIndex:0
    };


	  // - - - POST SCOPE
    var postScope = $scope.$new();
    postScope.betConfig = betConfig;
    postScope.newAnswer = '';
    postScope.showHelp = commonService.showHelp;
    postScope.betSetup = commonService.bets;


    postScope.onAnswerChange =function(){
      postScope.task.newAnswer = postScope.task.newAnswer.toUpperCase().replace(/[^А-ЯA-Z0-9_]/g,'')
    };

    postScope.changeBet = function(){
      betConfig.betIndex++;
      if(betConfig.betIndex>=betConfig.bets.length){betConfig.betIndex=0;}
      betConfig.postPrice = betConfig.basePostPrice*betConfig.bets[betConfig.betIndex].mult;
      betConfig.bet = betConfig.bets[betConfig.betIndex].id;
    };

    postScope.sendAnswer = function(){
      if(postScope.task.isBlocked){return;}
      if(!$scope.user.isPro && postScope.betSetup[postScope.betConfig.betIndex].isPro){
        $scope.showNeedProModal();
        return;
      }
      if(!postScope.task.newAnswer || postScope.task.newAnswer.length>30 || postScope.task.newAnswer.length<=0){
        return;
      };



      dataLayer.httpPost('/tasks/'+postScope.task._id+'/answer',false,{betId:betConfig.bet,answer:postScope.task.newAnswer}).then(function(res){

        if(res.data.code){
          if (res.data.code == 'NO_ENOUGH_COINS'){showWannaBuyDialog();}
          $ionicAnalytics.track('Error Post Answer', {
            taskId: postScope.task._id,
            betId: betConfig.bet,
            bet:  betConfig.postPrice,
            answer:postScope.task.newAnswer,
            error:res.data.code
          });
        }else{
          postScope.task.myAnswer = {answer:postScope.task.newAnswer, bet:betConfig.bet};

          $ionicAnalytics.track('Success Post Answer', {
            taskId: postScope.task._id,
            betId: betConfig.bet,
            bet:  betConfig.postPrice,
            answer:postScope.task.newAnswer
          });
          postScope.closeModal();
        }

      },function(err){
        $ionicAnalytics.track('Error Post Answer', {
          taskId: postScope.task._id,
          betId: betConfig.bet,
          bet:  betConfig.postPrice,
          answer:postScope.task.newAnswer,
          error:err
        });
        console.error(err);
      });

      $ionicAnalytics.track('Try Post Answer', {
        taskId: postScope.task._id,
        betId: betConfig.bet,
        bet:  betConfig.postPrice,
        answer:postScope.task.newAnswer
      });
    };

    postScope.closeModal = function() {
      fullscreenbannerService.showBanner();
      postScope.task=null;
      postScope.modal.hide();
    };

    $ionicModal.fromTemplateUrl('templates/post.modal.html', {
      scope: postScope
    }).then(function(modal) {
      postScope.modal = modal;
    });





    // - - - DETAILS SCOPE
    var detailsScope = $scope.$new();
    detailsScope.closeModal = function() {
      detailsScope.modal.hide();
    };

    detailsScope.cancelBet = function(){

    };

    $ionicModal.fromTemplateUrl('templates/card-details.modal.html', {
      scope: detailsScope
    }).then(function(modal) {
      detailsScope.modal = modal;
    });

    $scope.showTask = function(task){
      if(task.myAnswer){
        $scope.showDetails(task);
      }else{
        $scope.showPost(task);
      }
    };

    $scope.showDetails = function(task){
      detailsScope.task=task;
      detailsScope.myBet = _.findLast(task.bets,{mult:task.myAnswer.betId}).mult*task.setup.basePrice;
      console.log('!',task);
      detailsScope.modal.show();
    }

    $scope.showPost = function(task){
      betConfig.basePostPrice = task.setup.basePrice;
      betConfig.bets = task.bets;
      betConfig.postPrice = betConfig.basePostPrice*betConfig.bets[betConfig.betIndex].mult;
      betConfig.bet = betConfig.bets[betConfig.betIndex].id;
      postScope.task=task;
      fullscreenbannerService.prepareNext();
      postScope.modal.show();
    }


    $scope.gotoTest = function(){
         $state.go('intro')
    };

    var shopPopup;
    $scope.showNeedProModal = function() {
      $translate(['NEEDPRO_MODAL.BODY', 'NEEDPRO_MODAL.TITLE', 'NEEDPRO_MODAL.CANCEL_BUTTON', 'NEEDPRO_MODAL.SHOP_BUTTON']).then(function (translations) {
        shopPopup = $ionicPopup.show({
          template: translations['NEEDPRO_MODAL.BODY'],
          title: translations['NEEDPRO_MODAL.TITLE'],
          //subTitle: translations['NEEDPRO_MODAL.SUBTITLE'],
          scope: $scope,
          buttons: [
            {text: translations['NEEDPRO_MODAL.CANCEL_BUTTON']},
            {
              text: '<b class="white-text">' + translations['NEEDPRO_MODAL.SHOP_BUTTON'] + '</b>',
              type: 'button-positive',
              onTap: function (e) {
                if (!$scope.data.wifi) {
                  //don't allow the user to close unless he enters wifi password
                  e.preventDefault();
                } else {
                  return $scope.data.wifi;
                }
              }
            }
          ]
        });
      });
    };
    //shopPopup.then(function (res) {
    //  console.log('Thank you for not eating my delicious ice cream cone');
    //});



    function showWannaBuyDialog(){
      $translate(['CARD.DO_OPEN_SHOP_DIALOG.TITLE', 'CARD.DO_OPEN_SHOP_DIALOG.TEXT','CARD.DO_OPEN_SHOP_DIALOG.SUBTITLE','BUTTONS.CANCEL','CARD.DO_OPEN_SHOP_DIALOG.BUTTON_ACTION']).then(function (translations) {
        var myPopup = $ionicPopup.show({
          template: translations['CARD.DO_OPEN_SHOP_DIALOG.TEXT'],
          title: translations['CARD.DO_OPEN_SHOP_DIALOG.TITLE'],
          subTitle: translations['CARD.DO_OPEN_SHOP_DIALOG.SUBTITLE'],
          scope: $scope.$new(),
          buttons: [
            { text: translations['BUTTONS.CANCEL'] },
            {
              text: '<b>'+translations['CARD.DO_OPEN_SHOP_DIALOG.BUTTON_ACTION']+'</b>',
              type: 'button-positive',
              onTap: function(e) {
                postScope.closeModal();
                $state.go('app.profile');
              }
            }
          ]
        });


        //var confirmPopup = $ionicPopup.confirm({
        //  title: translations['CARD.DO_OPEN_SHOP_DIALOG.TITLE'],
        //  template: translations['CARD.DO_OPEN_SHOP_DIALOG.TEXT']
        //});
		//
		//confirmPopup.then(function(res) {
         // if(res) {
         //   //TODO GOTOSHOP
         //   $state.go('app.profile')
         // } else {
         //   console.log('You are not sure');
         // }
		//});
      });

    }

    function getTasks(){
      console.log('get TASKS');
        dataLayer.httpGet('/tasks', false).then(function(res){
        if(res.data.error || res.error){
          return console.error(res);
        }else{
          var receivedTime = new Date().getTime();

          if($scope.user.isPro){
            $scope.tasks = _.chain(res.data)
                .chunk(4)
                .each(function(chunk){
                  task.receivedTime = receivedTime;
                  chunk.push({advert:'ok',id:Math.floor(Math.random()*999999)});
                })
                .flatten()
                .union()
                .value();
          }else{
            $scope.tasks = _.each(res.data,function(task){
                  task.url = imageHost+task.image;
              console.log('TASK URL ',task.url)
                  task.receivedTime = receivedTime;
                });
          }
        }
      });
    }

    function getUnread(){
      dataLayer.httpGet('/me/unread', false).then(function(res){
        if(_.isNumber(+res.data) && !_.isNaN(+res.data)){
          $scope.unread = res.data;
        }
      });
    }

    getTasks();
    getUnread();

}]);
