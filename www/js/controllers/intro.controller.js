angular.module('starter').controller('IntroCtrl',['$scope', '$state',  'dataLayer','$ionicModal','$cordovaOauth','userService','commonService', function($scope, $state, dataLayer, $ionicModal,$cordovaOauth,userService,commonService) {


	var options = {};

	$scope.user = userService.getUser();
	$scope.signInFb = function(){
		dataLayer.signInFb(finishAuth);
	};
	$scope.signInVk = function(){
		dataLayer.signInVk(finishAuth);
	};

	$scope.signInNaked = function(){
		dataLayer.singInNaked(finishAuth);
	};

	function finishAuth(token) {
		userService.setToken(token);
		$state.go('app.cards');
	}
}]);