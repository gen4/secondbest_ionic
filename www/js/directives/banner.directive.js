angular.module('starter').directive('banner',['lodash', function(_) {
	return {
		restrict: 'E',
		scope: {},
		templateUrl: 'js/directives/banner.tmpl.html',
		link: function (scope, element) {
			(adsbygoogle = window.adsbygoogle || []).push({})
			scope.$on('$destroy', function () {

			})
		}
	};
}]);
