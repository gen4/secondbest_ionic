angular.module('starter').directive('countDown',['lodash','$interval', function(_,$interval) {
	return {
		restrict: 'E',
		scope: {
			task:'='
		},
		templateUrl: 'js/directives/countdown.tmpl.html',
		link: function (scope, element) {
			var interval;
			var states = [
				{val:180,class:'calm-bg'},
				{val:30,class:'energized-bg'},
				{val:0,class:'assertive-bg'},
				{val:-1,class:'stable-bg'}
			];
			scope.$watch('task',function(nv,ov){
				if(nv){
					start();
				}else{
					stop()
				}
			});
			function start(){
				//
				scope.seconds = scope.task.closedAfter - (new Date().getTime() - new Date(scope.task.receivedTime).getTime()) - 5;
				renderText();
				interval = $interval(function(){
					scope.seconds--;
					if(scope.seconds<=0){stop();}
					renderText();
				},999);
			}
			function stop(){
				if(interval){$interval.cancel(interval);}
			}
			function renderText(){
				if(scope.seconds<=0){
					scope.secondsFormated = '-';
					scope.barclass=states[3].class;
					scope.task.isBlocked=true;
					return;
				}
				for(var a=0;a<states.length;a++){
					if(scope.seconds>=states[a].val){
						scope.barclass=states[a].class;
						break;
					}
				}
				scope.secondsFormated = '';
				var sec_num = parseInt(scope.seconds, 10); // don't forget the second param
				var hours   = Math.floor(sec_num / 3600);
				var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
				var seconds = sec_num - (hours * 3600) - (minutes * 60);

				if (hours   < 10) {hours   = "0"+hours;}
				if (minutes < 10) {minutes = "0"+minutes;}
				if (seconds < 10) {seconds = "0"+seconds;}
				scope.secondsFormated =  hours+':'+minutes+':'+seconds;
			}
			//
			scope.$on('$destroy', function () {
				stop();
			})
		}
	};
}]);
