angular.module('starter').directive('taskCard',['lodash', function(_) {
  return {
    restrict: 'E',
    scope: {
      task: '='
    },
    templateUrl: 'js/directives/taskcard.tmpl.html',
    link: function (scope, element) {
      scope.$on('$destroy', function () {

      })
    }
  };
}]);
