angular.module('starter').factory('fullscreenbannerService', ['commonService','lodash','userService', function(commonService,_,userService) {
	var admobid = {};
	var isPrepairing = false;
	var isPrepared = false;
	var user = userService.getUser();
	function onDeviceReady(){

		// select the right Ad Id according to platform
		if( /(android)/i.test(navigator.userAgent) ) {
			admobid = { // for Android
				//banner: 'ca-app-pub-6869992474017983/9375997553',
				interstitial: 'ca-app-pub-4108475993632333/5620538883'
			};
		} else if(/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
			admobid = { // for iOS
				//banner: 'ca-app-pub-6869992474017983/4806197152',
				interstitial: 'ca-app-pub-6869992474017983/7563979554'
			};
		} else {
			admobid = { // for Windows Phone
				//banner: 'ca-app-pub-6869992474017983/8878394753',
				interstitial: 'ca-app-pub-6869992474017983/1355127956'
			};
		}

	}
	function prepareNext(){
		if(isPrepairing){return}
		if(isPrepared){return}
		if(user.isPro){return;}
		try{
			if(AdMob) AdMob.prepareInterstitial( {adId:admobid.interstitial, autoShow:false} );
			isPrepairing = true;
		}
		catch(e){
			console.error('no admob installed ',e)
		}

	}

	function showBanner(){
		if(user.isPro){return;}
		try {
			if (AdMob) AdMob.showInterstitial();
		}
		catch(e){
			console.error('no admob installed ',e)
		}
	}

	document.addEventListener('onAdFailLoad', function(e){
		isPrepairing=false;
	});
	document.addEventListener('onAdLoaded', function(e){
		isPrepairing=false;
		isPrepared = true;
	});
	document.addEventListener('onAdPresent', function(e){
		isPrepairing=false;
		isPrepared = false;
	});

	document.addEventListener('onAdDismiss', function(e){
		isPrepairing=false;
		isPrepared = false;
		prepareNext();
	});
	document.addEventListener('onAdLeaveApp', function(e){
		isPrepairing=false;
		isPrepared = false;
		prepareNext();
	});

	return {
		onDeviceReady:onDeviceReady,
		prepareNext:prepareNext,
		showBanner:showBanner
	}
}]);