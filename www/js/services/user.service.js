angular.module('starter').factory('userService', ['$ionicLoading','$http','$q','dataLayer','$state' ,function( $ionicLoading, $http, $q,dataLayer,$state) {

  document.addEventListener("deviceready", onDeviceReady, false);

  var user = {};
  var ionicUser;

  function onDeviceReady() {
    //console.log(device.cordova);
    //console.log('device.uuid;',device,device.uuid)
    //user.device = device;
    console.log('on device ready');
    ionicUser = Ionic.User.current();
    Ionic.io();//need for ionic user
    var sbSavedUser = localStorage.getItem("sbSavedUser");
    if(sbSavedUser) {
      try {
        user = JSON.parse(sbSavedUser);
        if(user.token){setToken(user.token)};
        if(user.regs && user.regs[0] && user.regs[0].token){setToken(user.regs[0].token)}
        if(user.authenticated){$state.go('app.cards');}

      } catch (e) {
        console.error('saved user parsing error ');
      }
    }

  }

  function saveLocalUser(){
    //if we have a token - save user
    if(user.token || (user.regs && user.regs[0] && user.regs[0].token)){
      ionicUser.id =  user.token || user.regs[0].token;
      ionicUser.set('coins',user.coins);
      ionicUser.set('isPro',user.isPro);
      ionicUser.set('isPaid',(user.purcheses && user.purcheses.length>0));
      if(user.stats){ionicUser.set('games',user.stats.games);}
      ionicUser.save();
    }
    localStorage.setItem("sbSavedUser", JSON.stringify(user));
  }

  function setToken(token){
    console.log('set token')
    $http.defaults.headers.common = { 'token' : token };
    $http.defaults.xsrfCookieName = 'token';
    $http.defaults.withCredentials = true;
    user.authenticated=true;
    if(user.regs){
      user.regs = [{token:token}];
    }else{
      user.token = token;
    }
    //saveLocalUser();
    refreshUserData().then(function(){
      saveLocalUser();
    });
  };




  function refreshUserData(){
    console.log('refreshUserData')
    return $q(function(resolve, reject) {
      dataLayer.httpGet('/authenticate', false).then(function(res){
        console.log('user got', res);
        if(res.data.user){
          user.authorized=true;
          user = res.data.user;
          resolve(user)
        }else{
          reject('no user recieved')
        }
      },function(err){
        console.log('user error', err);
        reject(err);
      });
    });
  }

  function logout(){
    user = {};
    $state.go('intro');
  }


  return {
    logout:logout,
    getUser:function(){
      if(_.isEmpty(user)){
        onDeviceReady();
        return user;
      }else{
        return user;
      }

    },
    setToken: setToken,
    refreshUserData:refreshUserData
  }

}]);
