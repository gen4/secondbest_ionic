angular.module('starter').factory('notificationsService', ['$cordovaLocalNotification','$translate', function( $cordovaLocalNotification,$translate) {
	var localId = "31337";

	function initLocalNotifications(){

		$cordovaLocalNotification.isScheduled(localId).then(function(isScheduled) {
			clearAllLocalNotifications();
			setLocalNotifications([
				{
					delayHours:24,
					title:'NOTIFICATIONS.FIRST.TITLE',
					message:'NOTIFICATIONS.FIRST.MESSAGE'

				},
				{
					delayHours:48,
					title:'NOTIFICATIONS.FIRST.TITLE',
					message:'NOTIFICATIONS.FIRST.MESSAGE'

				},
				{
					delayHours:71,
					title:'NOTIFICATIONS.FIRST.TITLE',
					message:'NOTIFICATIONS.FIRST.MESSAGE'

				},
				{
					delayHours:119,
					title:'NOTIFICATIONS.FIRST.TITLE',
					message:'NOTIFICATIONS.FIRST.MESSAGE'

				}
			]);
		});
	}

	function clearAllLocalNotifications(){
		$cordovaLocalNotification.cancelAll();
	}

	function setLocalNotifications(data){
		var now = new Date().getTime();
		var shiftTime=0;//1000*60*15
		var then = now;
		_.each(data,function(d,i){
			then += d.delayHours*1000-shiftTime;//*60*60
			$translate([d.title, d.message]).then(function (translations) {
				$cordovaLocalNotification.add({
					id: "local_" + i,
					date: then,
					message: translations[d.message],
					title: translations[d.title],
					autoCancel: true,
					sound: null
				}).then(function () {
					console.log("The notification [%d] has been set", i);
				});
			})
		})
	}

	return {
		initLocalNotifications:initLocalNotifications
	}
}]);