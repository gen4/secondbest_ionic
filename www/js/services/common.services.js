angular.module('starter').factory('commonService', ['$ionicPopup','$translate','$rootScope','$ionicModal', function($ionicPopup,$translate, $rootScope,$ionicModal) {
	function showErrorAlert(msg) {
		var code= 'ERROR_MODAL.'+msg;
		$translate(['ERROR_MODAL.TITLE',code]).then(function (translations) {
			var alertPopup = $ionicPopup.alert({
				title: translations['ERROR_MODAL.TITLE'],
				template: translations[code]
			});
			alertPopup.then(function (res) {
				//console.log('Thank you for not eating my delicious ice cream cone');
			});
		});
	}

	function showErrorAlertNoTranslation(msg){
		$translate(['ERROR_MODAL.TITLE']).then(function (translations) {
			var alertPopup = $ionicPopup.alert({
				title: translations['ERROR_MODAL.TITLE'],
				template: msg
			});
			alertPopup.then(function (res) {
				//console.log('Thank you for not eating my delicious ice cream cone');
			});
		});
	}

	var helpScope = $rootScope.$new();
	helpScope.closeModal = function() {
		helpScope.modal.hide();
	};
	$ionicModal.fromTemplateUrl('templates/help.modal.html', {
		scope: helpScope
	}).then(function(modal) {
		helpScope.modal = modal;
	});
	function showHelp(){
		console.log('h s m');
		helpScope.modal.show();
	}

	return {
		showErrorAlert:showErrorAlert,
		showHelp:showHelp,
		showErrorAlertNoTranslation:showErrorAlertNoTranslation,
		bets:[
			{class:'button-calm', mult:1},
			{class:'button-positive', mult:3},
			{class:'button-royal', mult:10}
		]
	}
}]);


