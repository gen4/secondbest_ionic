angular.module('starter').factory('purchasesService', ['commonService','lodash','$ionicAnalytics', function(commonService,_,$ionicAnalytics) {

	//document.addEventListener("deviceready", onDeviceReady, false);

	var isStoreReady = false;

	var items = {}; // price, id, description, title, alias, loaded, vaild

	function onDeviceReady() {
		if (!window.store) {
			console.error('Store not available');
			commonService.showErrorAlert('NO_STORE')
			return;
		}

		store.verbosity = store.DEBUG;
//		store.validator = "https://api.fovea.cc:1982/check-purchase";

		registerCoins('coins0');
		registerCoins('coins_pro1');
		registerCoins('coins_pro2');
		store.refresh();

		store.error(function(error) {
			console.error('ERROR ' + error.code + '::: ' + error.message);
			commonService.showErrorAlert('NO_STORE');
		});


		store.when("product").updated(function (p) {
			console.log('!!! UPDATE ',p);
			var item = _.findLast(items,{id: p.id});
			if(item){
				_.assign(item,p);
			}else{
				items[p.alias] = p
			}
		});

		store.ready(function() {
			isStoreReady = true;
		})

	}


	function registerCoins(pid){
		store.register({
			id:    'com.laval.secondbest.'+pid, // id without package name!
			alias: pid,
			type:   store.CONSUMABLE
		});
		var p = store.get(pid);
		store.when(pid).approved(function (order) {
			log("You got ",pid);
			$ionicAnalytics.track('Success Purchase Item', {
				item: p
			});
			items[pid] = order;
			order.finish();
		});

		store.when(pid).cancelled(function (order) {
			$ionicAnalytics.track('Canceled Purchase Item', {
				item: p
			});
		});

		store.when(pid).error(function (order) {
			log("You got ",pid);
			$ionicAnalytics.track('Error Purchase Item', {
				item: p
			});
		});
	}


	return{
		isReady:function(){
			return isStoreReady;
		},
		refresh:function(){
			if(isStoreReady){
				store.refresh();
			}else{
				console.error('refresh impossible. store is not ready')
			}
		},
		order:function(pid){
			console.log('order',pid);
			var p = store.get(pid);
			$ionicAnalytics.track('Try Purchase Item', {
				item: p
			});
			store.order(pid);
		},
		onDeviceReady:onDeviceReady
	}
}]);
