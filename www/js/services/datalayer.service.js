angular.module('starter').factory('dataLayer', ['$ionicLoading','$http','$q', '$rootScope','$ionicPopup','commonService', '$cordovaOauth',
  function( $ionicLoading, $http, $q,$rootScope,$ionicPopup,commonService,$cordovaOauth) {
  //var baseUrl = 'http://192.168.99.100:3000/api/v1';
    var baseUrl = 'https://2ndbest.scalingo.io/api/v1';

  var cache = {};

  var fb_app_id = '146963655688290';
  var vk_app_id = '5306843';
  var appScope = ["email"];





  function successResponseHandler(url, hash, shouldCached){

    return function(result){
      console.log('http [%s] result',url,result);
      $ionicLoading.hide();
      if(result.data.error){
        //commonService.showErrorAlertNoTranslation(result.data.error.text)
        errorResponseHandler(url)(result.data.error);
      }else{
        if(shouldCached){
          cache[hash] = result;
        }
      }
    }
  }

  function httpRequest(method, url, shouldCached, data){
    if(!data){data = {}}
    var hash = url+':'+JSON.stringify(data);

    if(shouldCached && cache[hash]){
      return  $q(function(resolve, reject) {resolve(cache[hash])});
    }else {
      $ionicLoading.show({template: 'Loading...'});
      var p;
      if(method=='GET') {
        p  = $http({method: 'GET', url: baseUrl+url, params:data});
      }else if(method=='POST') {
        p  = $http({method: 'POST', url: baseUrl+url, data:data});
      }
      p.then(successResponseHandler(url,hash, shouldCached),errorResponseHandler(url));
      return p;
    }
  }

  function errorResponseHandler(url){
    return function(err){
        console.error(url,err);
      commonService.showErrorAlertNoTranslation(err.text);
        $ionicLoading.hide();
    }
  }

  function finishAuth(service, done) {
    return function (res) {
      console.log('success', res);
      httpRequest('POST','/me/registration/' + service, false, res).then(function (token) {
        if(token && token.data && token.data.token){
          done(token.data.token);
        }else{
          done(null);
        }
      })
    }
  };
    function finishAddService(service, done) {
      return function (res) {
        console.log('success', res);
        httpRequest('POST','/me/addService/' + service, false, res).then(function (token) {
          if(token && token.data && token.data.token){
            done(token.data.token);
          }else{
            done(null);
          }
        })
      }
    };


  return {
    httpGet:function(url, shouldCached,data){
      return httpRequest('GET', url, shouldCached, data);
    },
    httpPost:function(url, shouldCached,data){
      return httpRequest('POST', url, shouldCached, data);
    },
    setUserPersonalData:function(data){
      if(options){
        pbUser.setUserOptions(data);
      }
    },
    signInFb:function(done){
      console.log('fb in');
      $cordovaOauth.facebook(fb_app_id,appScope,options).then(finishAuth('fb',done),function(err){
        console.error('error',err)
      });
    },
    signInVk:function(done) {
      console.log('vk in');
      $cordovaOauth.vkontakte(vk_app_id, appScope).then(finishAuth('vk', done), function (err) {
        console.error('error', err)
      });
    },
    singInNaked:function(done){
      try {
        console.log('naked in',device);
        finishAuth('naked',done)({UDID:device.uuid});
      }
      catch (e) {
        console.log('naked in','RND');
        finishAuth('naked',done)({UDID:'RND_'+Math.floor(Math.random()*99999999)});
      }
    },
    addFb:function(done){
      console.log('fb in');
      $cordovaOauth.facebook(fb_app_id,appScope,options).then(finishAddService('fb',done),function(err){
        console.error('error',err)
      });
    },
    addVk:function(done) {
      console.log('vk in');
      $cordovaOauth.vkontakte(vk_app_id, appScope).then(finishAddService('vk', done), function (err) {
        console.error('error', err)
      });
    },
  };
}]);
