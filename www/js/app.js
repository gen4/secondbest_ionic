// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
  'ionic','ionic.service.core',
  'ionic.service.analytics',
  'ngCordova',
  'angularMoment',
  'pascalprecht.translate',
  'ngCordovaOauth',
  'ngLodash'
])

.run(function($ionicPlatform,$ionicAnalytics, $rootScope, $state, $ionicModal, dataLayer, purchasesService, userService, fullscreenbannerService,notificationsService) {

  $rootScope.$on("$routeChangeStart", function(evt, to, from) {
        if (to.authorize === true && !userService.user.authenticated) {
          $location.path("/intro");
        }else if(to.unauthorize === true && userService.user.authenticated){
          $location.path("/app/cards");
        }
  });

  $ionicPlatform.ready(function() {
    $ionicAnalytics.register();
    $ionicAnalytics.setGlobalProperties({
      app_version_number: 'v0.0.7',
      day_of_week: (new Date()).getDay()
    });
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    purchasesService.onDeviceReady();
    fullscreenbannerService.onDeviceReady()
    notificationsService.initLocalNotifications()
  });


})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $translateProvider) {

  $httpProvider.defaults.timeout = 3000;

    $translateProvider.useStaticFilesLoader({
      prefix: 'l10n/locale-',
      suffix: '.json'
    })
    .preferredLanguage('ru');

  $stateProvider
    .state('intro',{
      url:'/intro',
      unauthorize:true,
      templateUrl: 'templates/intro.page.html',
      controller: 'IntroCtrl',
      resolve:{
        version:checkVersion
      }
    })

    .state('app', {
    url: '/app',
    abstract: true,
    authorize:true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl',
      resolve:{
        current_user:function(userService){
          return userService.getUser();
        },
        version:checkVersion
      }
  })

  .state('app.cards', {
    url: '/cards',
    cache:false,
    authorize:true,
    views: {
      'menuContent': {
        controller: 'CardsCtrl',
        templateUrl: 'templates/cards.page.html'
        //resolve:{

          //unread:function(dataLayer){
          //
          //}
        //}
      }

    }
  })
  .state('app.profile', {
    url: '/profile',
    cache: false,
    authorize:true,
    views: {
      'menuContent': {
        controller: 'ProfileCtrl',
        templateUrl: 'templates/profile.page.html',
        resolve:{
            //current_user:function(userService){
            //  return
            //},
            rating_position:function(dataLayer){
              return dataLayer.httpGet('/me/ratingPosition', false)
            }
        }
      }
    }
  })
  .state('app.history', {
    url: '/ratings',
    cache:false,
    authorize:true,
    views: {
      'menuContent': {
        controller: 'HistoryCtrl',
        templateUrl: 'templates/history.page.html'
      }
    }
  });


  function checkVersion(dataLayer,$ionicPopup,$translate){
    var clientVersion = 0;
    dataLayer.httpGet('/version', true).then(function(res){
      if(res.data.CLIENT.split('.')[0]<=clientVersion) {
        //all ok
        console.log('VERSION OK '+res.data.CLIENT);
        return res.data;
      }else{
        console.log('VERSION CONTROL ISSUE '+res.data.CLIENT);
        $translate(['VERSION_MODAL.BODY', 'VERSION_MODAL.TITLE','VERSION_MODAL.BUTTON']).then(function (translations) {
          $ionicPopup.show({
            title: translations['VERSION_MODAL.TITLE'],
            template: translations['VERSION_MODAL.BODY'],
            buttons: [
              {
                text: translations['VERSION_MODAL.BUTTON'],
                type: 'button-positive',
                onTap: function(e) {
                  window.open('http://www.ya.ru', '_system', 'location=yes');
                  e.preventDefault();
                }
              }
            ]
          });
        });
      }
    })
  }


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/intro');
});




